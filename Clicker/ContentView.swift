//
//  ContentView.swift
//  Clicker
//
//  Created by Enrick Enet on 31/12/2020.
//

import SwiftUI

struct ContentView: View {
    @State var score = 0
    @State var gameIsInProgress = false
    @State var resultList:[GameResult] = []
    @AppStorage("nickName") var nickName = ""
    @AppStorage("bestScore") var bestScore = 0
    @AppStorage("bestNickName") var bestNickName = ""
    
    var body: some View {
        VStack {
            EditableTextView(editedText: $nickName, title: "Pseudo")
                .padding()
            HStack {
                if score >= bestScore && score != 0 {
                    Image(systemName: "flame")
                }
                Text("Score : \(score)")
            }.font(.title)
            if bestScore > 0 {
                HStack{
                    Image(systemName: "star")
                    Text("\(bestNickName) - \(bestScore)")
                    Image(systemName: "star")
                }
            }
            
            if gameIsInProgress == true {
                Image(systemName: "plus.square")
                    .font(.title)
                    .onTapGesture { userTouchedClickButton() }
            }
            GameresultListView(resultList: resultList)
            Spacer()
            if gameIsInProgress == false {
                Button("Nouvelle Partie"){
                    userTouchedStartButton()
                }.padding()
            }
        }
    }
    
    func userTouchedStartButton() {
        score = 0
        gameIsInProgress = true
        Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false) { (_) in
            gameDidFinish()
        }
    }
    
    func gameDidFinish() {
        gameIsInProgress = false
        if score > bestScore {
            bestScore = score
            bestNickName = nickName
        }
        let result = GameResult(playerName: nickName, score: score)
        resultList.append(result)
    }
    
    func userTouchedClickButton() {
        score += 1
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
