//
//  GameresultListView.swift
//  Clicker
//
//  Created by Enrick Enet on 10/01/2021.
//

import SwiftUI

struct GameresultListView: View {
    let resultList:[GameResult]
    var body: some View {
        List(resultList) { (result:GameResult) in 
            Text("\(result.playerName) - \(result.score)")
        }
    }
}

struct GameresultListView_Previews: PreviewProvider {
    static var previews: some View {
        GameresultListView(resultList: [
            GameResult(playerName: "Zephy*", score: 73),
            GameResult(playerName: "Netnet 69*", score: 69),
            GameResult(playerName: "Barbupapa17", score: 100)
        ])
        .previewLayout(.sizeThatFits)
    }
}
