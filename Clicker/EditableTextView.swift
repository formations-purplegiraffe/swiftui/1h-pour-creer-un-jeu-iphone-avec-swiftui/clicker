//
//  EditableTextView.swift
//  Clicker
//
//  Created by Enrick Enet on 08/01/2021.
//

import SwiftUI

struct EditableTextView: View {
    @Binding var editedText: String
    @State private var isEditing = true
    let title:String
    
    var body: some View {
        HStack{
            if isEditing == true {
                TextField(title, text: $editedText)
                    .frame(width: 300.0)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
            } else {
                Text(editedText)
                    .frame(width: 300.0)
            }
            Image(systemName: "pencil.circle")
                .onTapGesture {
                    isEditing.toggle() //Le toggle permet de modifier le bool
                }
        }
    }
}

struct EditableTextView_Previews: PreviewProvider {
    static var previews: some View {
        EditableTextView(editedText: .constant("Enrick"), title: "Pseudo")
            .previewLayout(.sizeThatFits)
            
        EditableTextView(editedText: .constant("Enrick"), title: "Pseudo")
            .preferredColorScheme(.dark)
            .previewLayout(.sizeThatFits)
    }
}
