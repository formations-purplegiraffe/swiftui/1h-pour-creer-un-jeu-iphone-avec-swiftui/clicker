//
//  GameResult.swift
//  Clicker
//
//  Created by Enrick Enet on 10/01/2021.
//

import Foundation

struct GameResult : Identifiable {
    let id:UUID = UUID()
    let playerName:String
    let score:Int
}
